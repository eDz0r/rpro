﻿using GrandTheftMultiplayer.Server.API;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace servertools
{
    public partial class servertoolsForm : Form
    {
		private API API;
		
        public servertoolsForm(API api)
        {
            InitializeComponent();

			API = api;

            UpdateResourceLists();
        }

        private void UpdateResourceLists()
        {
            listviewResources.Items.Clear();
            foreach (var resource in API.getAllResources())
            {
                if (resource != "servertools")
                {
                    ListViewItem item = new ListViewItem(resource);
                    item.SubItems.Add(API.isResourceRunning(resource).ToString().ToLower());
                    listviewResources.Items.Add(item);
                }
            }
        }

        private void buttonResourceStartRestart_Click(object sender, EventArgs e)
        {
            if (listviewResources.SelectedItems.Count > 0)
            {
                for (var i = 0; i < listviewResources.SelectedItems.Count; i++)
                {
                    var resource = listviewResources.SelectedItems[i].SubItems[0].Text;
                    if (API.isResourceRunning(resource))
                    {
                        API.stopResource(resource); API.startResource(resource);
                    }
                    else if (!API.isResourceRunning(resource))
                    { 
                        API.startResource(resource);
                    }
                }
            }
            UpdateResourceLists();
        }

        private void buttonResourceStop_Click(object sender, EventArgs e)
        {
            if (listviewResources.SelectedItems.Count > 0)
            {
                for (var i = 0; i < listviewResources.SelectedItems.Count; i++)
                {
                    var resource = listviewResources.SelectedItems[i].SubItems[0].Text;
                    if (API.isResourceRunning(resource))
                    { 
                        API.stopResource(resource);
                    }
                }
            }
            UpdateResourceLists();
        }

        private void buttonResourceRefresh_Click(object sender, EventArgs e)
        {
            UpdateResourceLists();
        }
    }
}