﻿using System;
using System.Windows.Forms;
using System.Threading;
using GrandTheftMultiplayer.Server.API;

namespace servertools
{
    public class servertoolsMain : Script
    {
        private servertoolsForm servertools = null;

        public servertoolsMain()
        {
            new Thread(() =>
            {
                while (true)
                {
                    servertools = new servertoolsForm(API);
                    Application.Run(servertools);
                }

            }).Start();
        } 
    }
}