﻿namespace servertools
{
    partial class servertoolsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelResources = new System.Windows.Forms.Label();
            this.listviewResources = new System.Windows.Forms.ListView();
            this.columnName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnRunning = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonResourceStartRestart = new System.Windows.Forms.Button();
            this.buttonResourceStop = new System.Windows.Forms.Button();
            this.buttonResourceRefresh = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelResources
            // 
            this.labelResources.AutoSize = true;
            this.labelResources.Location = new System.Drawing.Point(12, 9);
            this.labelResources.Name = "labelResources";
            this.labelResources.Size = new System.Drawing.Size(61, 13);
            this.labelResources.TabIndex = 0;
            this.labelResources.Text = "Resources:";
            // 
            // listviewResources
            // 
            this.listviewResources.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnName,
            this.columnRunning});
            this.listviewResources.Location = new System.Drawing.Point(15, 26);
            this.listviewResources.Name = "listviewResources";
            this.listviewResources.Size = new System.Drawing.Size(230, 180);
            this.listviewResources.TabIndex = 1;
            this.listviewResources.UseCompatibleStateImageBehavior = false;
            this.listviewResources.View = System.Windows.Forms.View.Details;
            // 
            // columnName
            // 
            this.columnName.Text = "Name";
            this.columnName.Width = 136;
            // 
            // columnRunning
            // 
            this.columnRunning.Text = "Running";
            // 
            // buttonResourceStartRestart
            // 
            this.buttonResourceStartRestart.Location = new System.Drawing.Point(15, 212);
            this.buttonResourceStartRestart.Name = "buttonResourceStartRestart";
            this.buttonResourceStartRestart.Size = new System.Drawing.Size(70, 25);
            this.buttonResourceStartRestart.TabIndex = 2;
            this.buttonResourceStartRestart.Text = "(Re)Start";
            this.buttonResourceStartRestart.UseVisualStyleBackColor = true;
            this.buttonResourceStartRestart.Click += new System.EventHandler(this.buttonResourceStartRestart_Click);
            // 
            // buttonResourceStop
            // 
            this.buttonResourceStop.Location = new System.Drawing.Point(95, 212);
            this.buttonResourceStop.Name = "buttonResourceStop";
            this.buttonResourceStop.Size = new System.Drawing.Size(70, 25);
            this.buttonResourceStop.TabIndex = 3;
            this.buttonResourceStop.Text = "Stop";
            this.buttonResourceStop.UseVisualStyleBackColor = true;
            this.buttonResourceStop.Click += new System.EventHandler(this.buttonResourceStop_Click);
            // 
            // buttonResourceRefresh
            // 
            this.buttonResourceRefresh.Location = new System.Drawing.Point(175, 212);
            this.buttonResourceRefresh.Name = "buttonResourceRefresh";
            this.buttonResourceRefresh.Size = new System.Drawing.Size(70, 25);
            this.buttonResourceRefresh.TabIndex = 4;
            this.buttonResourceRefresh.Text = "Refresh";
            this.buttonResourceRefresh.UseVisualStyleBackColor = true;
            this.buttonResourceRefresh.Click += new System.EventHandler(this.buttonResourceRefresh_Click);
            // 
            // servertoolsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(257, 241);
            this.Controls.Add(this.buttonResourceRefresh);
            this.Controls.Add(this.buttonResourceStop);
            this.Controls.Add(this.buttonResourceStartRestart);
            this.Controls.Add(this.listviewResources);
            this.Controls.Add(this.labelResources);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "servertoolsForm";
            this.ShowIcon = false;
            this.Text = "GT-MP server tools";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelResources;
        private System.Windows.Forms.ListView listviewResources;
        private System.Windows.Forms.ColumnHeader columnName;
        private System.Windows.Forms.ColumnHeader columnRunning;
        private System.Windows.Forms.Button buttonResourceStartRestart;
        private System.Windows.Forms.Button buttonResourceStop;
        private System.Windows.Forms.Button buttonResourceRefresh;
    }
}

