﻿using System;
using GrandTheftMultiplayer.Server;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using System.Collections.Generic;

namespace RP_RO.server
{
    public class main : Script
    {
        public main()
        {
            // Handlers
            API.onResourceStart += onResourceStartHandler;
            API.onResourceStop += onResourceStopHandler;
            API.onPlayerConnected += onPlayerConnectedHandler;
            API.onPlayerDisconnected += onPlayerDisconnected;
            API.onPlayerFinishedDownload += onPlayerFinishedDownloadHandler;
        }

        public void onResourceStartHandler()
        {
            // Some credits...
            API.consoleOutput(" ");
            API.consoleOutput("+--------------------------------------+");
            API.consoleOutput("| Script name: RolePlay Romania.       |");
            API.consoleOutput("| Version: 1.0(build 1)                |");
            API.consoleOutput("| By: Constantin (eDz0r) Eduard,       |");
            API.consoleOutput("|     Iatagan (NeeR) Gabriel.          |");
            API.consoleOutput("+--------------------------------------+");
            API.consoleOutput(" ");
        }

        public void onResourceStopHandler()
        {

        }

        private void onPlayerConnectedHandler(Client player)
        {
            string stringPlayerName = API.getPlayerName(player);

            API.sendChatMessageToPlayer(player, misc.CHATCOLOR_SYSTEMCONNECT + "[SERVER]: Welcome to " + misc.SERVER_NAME + misc.CHATCOLOR_SYSTEMCONNECT + ".");
        }

        private void onPlayerDisconnected(Client player, string reason)
        {

        }

        private void onPlayerFinishedDownloadHandler(Client player)
        {

        }
    }
}
