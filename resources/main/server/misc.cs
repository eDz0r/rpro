﻿using System;
using System.Collections.Generic;
using GrandTheftMultiplayer.Server;
using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;

namespace RP_RO.server
{
    class misc : Script
    {
        // Chat colors
        // Server
        public static string SERVER_NAME = "RolePlay Romania";
        // Basic colors
        public static string CHATCOLOR_RED = "~r~";
        public static string CHATCOLOR_BLUE = "~b~";
        public static string CHATCOLOR_GREEN = "~g~";
        public static string CHATCOLOR_YELLOW = "~y~";
        public static string CHATCOLOR_PURPLE = "~p~";
        public static string CHATCOLOR_PINK = "~q~";
        public static string CHATCOLOR_ORANGE = "~o~";
        public static string CHATCOLOR_GREY = "~c~";
        public static string CHATCOLOR_DARKERGREY = "~m~";
        public static string CHATCOLOR_BLACK = "~u~";
        public static string CHATCOLOR_DEFAULTWHITE = "~s~";
        public static string CHATCOLOR_WHITE = "~w~";
        // System
        public static string CHATCOLOR_SYSTEMCONNECT = "~o~";
        public static string CHATCOLOR_SYSTEMDISCONNECT = "~o~";
        public static string CHATCOLOR_SYSTEMSUCCESS = "~g~";
        public static string CHATCOLOR_SYSTEMERROR = "~r~";
        public static string CHATCOLOR_SYSTEMINFO = "~y~";
        public static string CHATCOLOR_SYSTEMHELP = "~y~";
        public static string CHATCOLOR_SYSTEMCOMMAND = "~w~";
        // Player
        public static string CHATCOLOR_PLAYERNAME = "~w~";
        public static string CHATCOLOR_PLAYERSTATS = "~w~";
        public static string CHATCOLOR_PLAYERLOCATION = "~w~";

        // Cameras
        // Register/Login screen
        public static Vector3 cameraLoginPos = new Vector3(707.2697, 977.2883, 357.8531);
        public static Vector3 cameraLoginRot = new Vector3(1.25417, 0.5413734, -2.318571);
        // Skin select
        public static Vector3 cameraCharacterCreationPos = new Vector3(403.1208, -999.4, -99.00403);
        public static Vector3 cameraCharacterCreationRot = new Vector3(0, 0, 0.0000);
        // Coordinates
        // Spawn
        public static Vector3 coordsSpawnPos = new Vector3(-1041.465, -2744.78, 21.3594);
        public static Vector3 coordsSpawnRot = new Vector3(0, 0, -35.0);
        // Skin select
        public static Vector3 coordsCharacterCreationPos = new Vector3(402.8786, -996.6062, -99.00026);
        public static Vector3 coordsCharacterCreationRot = new Vector3(0, 0, 180.0000);

        // Valid name characters
        static char[] validNameCharacters =
        {
            '!', '"', '#', '$', '%', '&', '(', ')', '*', '+',
            ',', '-', '.', '/', '0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', ':', ';', '<', '=', '>', '?',
            '@', '[', ']', '^', '_', '`', 'a', 'b', 'c', 'd',
            'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
            'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
            'y', 'z', '{', '|', '}', '~', '\"', '\''
        };

        char[] validPasswordCharacters = validNameCharacters;

        public misc()
        {
            // Handlers
            API.onResourceStart += onResourceStartHandler;
            API.onResourceStop += onResourceStopHandler;
            API.onClientEventTrigger += onClientEventHandler;
            API.onEntityEnterColShape += onEntityEnterColShapeHandler;
            API.onEntityExitColShape += onEntityExitColShapeHandler;
        }

        public void onResourceStartHandler()
        {

        }

        public void onResourceStopHandler()
        {

        }

        public void onClientEventHandler(Client player, string eventName, params object[] args)
        {

        }
        
        public void onEntityEnterColShapeHandler(ColShape colshape, NetHandle entity)
        {

        }

        public void onEntityExitColShapeHandler(ColShape colshape, NetHandle entity)
        {

        }

        public Client getVehicleDriver(NetHandle vehicleid)
        {
            List<Client> InVehicle = API.getAllPlayers().FindAll(x => x.vehicle.handle == vehicleid && x.vehicleSeat == -1);

            foreach(Client playerHandle in InVehicle)
            {
                return playerHandle;
            }
            return null;
        }

        public bool isNameValid(string name)
        {
            foreach (char character in name)
            {
                if (!isLetterInArray(character.ToString(), validNameCharacters)) { return false; }
            }
            return true;
        }

        public bool isPasswordValid(string name)
        {
            foreach (char character in name)
            {
                if (!isLetterInArray(character.ToString(), validPasswordCharacters)) { return false; }
            }
            return true;
        }

        public bool isEntityInRangeOf(Client entity, float x, float y, float z, float range)
        {
            Vector3 entityPosition = API.getEntityPosition(entity);

            if
            (
                (x >= (entityPosition.X - range)) && (x <= (entityPosition.X + range)) &&
                (y >= (entityPosition.Y - range)) && (y <= (entityPosition.Y + range)) &&
                (z >= (entityPosition.Z - range)) && (z <= (entityPosition.Z + range))
            ) 
            { return true; } else { return false; }
        }

        public bool isLetterInArray(string search, char[] input)
        {
            search = search.ToLower();

            foreach (char character in input) 
	        {
                if (search == character.ToString())
                {
                    return true;
                }
            }
            return false;
        }
    }
}